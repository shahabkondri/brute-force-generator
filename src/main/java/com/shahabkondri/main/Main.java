package com.shahabkondri.main;

import com.shahabkondri.generator.BruteForceGenerator;

import java.util.Scanner;

/**
 * Created by shahab on 4/28/15.
 */
public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a password that is up to 5 chars and contains no numbers: ");
        String pw = "" + scan.nextLine();

        for (int i = 7; i <= 7; i++) {
            BruteForceGenerator generator = new BruteForceGenerator(i);
            generator.forEachRemaining(test -> {
                if (pw.equals(test)) {
                    System.out.println("Your password: " + test);
                    System.exit(1);
                }
            });
        }
    }
}