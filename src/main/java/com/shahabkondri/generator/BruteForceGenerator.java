package com.shahabkondri.generator;

import java.util.Iterator;

/**
 * Created by shahab on 4/28/15.
 */
public class BruteForceGenerator implements Iterator<String> {

    private static final char[] CHOICES = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
//            '?', '.', ',', '!', '@', '#', ' ', '$', '^', '&', '(', ')', '_', '+', '=', '~', ';', ':'
    };

    private static final int MAX_INDEX = CHOICES.length - 1;
    private boolean keepProducing = true;
    private final int[] indexes;

    public BruteForceGenerator(final int length) {
        indexes = new int[length];
        initIndexes();
    }

    private void initIndexes() {
        for (int i = 0; i < indexes.length; i++) {
            indexes[i] = 0;
        }
    }

    @Override
    public boolean hasNext() {
        if (!keepProducing) {
            return false;
        }

        for (int index : indexes) {
            if (index < MAX_INDEX) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String next() {
        if (!keepProducing || !hasNext()) {
            return null;
        }

        String next = produceString();
        adjustIndexes();

        return next;
    }

    public void stop() {
        keepProducing = false;
    }

    private String produceString() {
        StringBuilder sb = new StringBuilder();
        for (int index : indexes) {
            sb.append(CHOICES[index]);
        }

        return sb.toString();
    }

    private void adjustIndexes() {
        int i;
        for (i = 0; i < indexes.length; i++) {
            if (indexes[i] < MAX_INDEX) {
                indexes[i] = indexes[i] + 1;
                break;
            }
        }
        for (int j = 0; j < i; j++) {
            indexes[j] = 0;
        }
    }
}
